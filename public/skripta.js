window.addEventListener('load', function() {
	//stran nalozena

	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	};

	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	};

	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);
	
	//pogon funkcije pridobivanja datotek
	
	
	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		//inicializacija
		var xhttp = new XMLHttpRequest();
		
		//kreirenjae requesta, in posiljanje
		xhttp.open('get','/datoteke',true);
		xhttp.send();
		
		console.log("request sent");
		
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);
	
				var datotekeHTML = document.querySelector("#datoteke");
				console.log("for loop");
				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];
						
					var velikost = datoteka.velikost;
					var enota = "B";
					
					 //preverjanje velikosti datoteke
					if(velikost > 1024) {
						enota = "KiB"
						velikost = Math.floor(velikost/1024);
                    } else if (velikost > 1024*1024) {
                        	enota = "MiB";
                            velikost = Math.floor(velikost/1024*1024);
                    } else if (velikost > 1024*1024*1024){
                            enota = "GiB";                                              
                            velikost = Math.floor(velikost/1024*1024*1024);
                    }
                                       
                    //dodajanje datotkere v html

					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + " " + enota + ") </div> \
							<div class='akcije'> \
							  <span><a href='/poglej/" + datoteka.datoteka + "' target='_blank'>Poglej</a></span> \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka + "'>Izbriši</span> </div> \
					    </div>";
				}

				if (datoteke.length > 0) {
					console.log("dodajanje funkcije brisi");
					var brisanje = document.querySelectorAll("span[akcija='brisi']");
					
					for(var i = 0; i < brisanje.length; i++)
						brisanje[i].addEventListener("click", brisi);
				}
				ugasniCakanje();
			}
		};
	};

	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		
		//request
		xhttp.open("GET", "/brisi/" + this.getAttribute("datoteka"), true);
		xhttp.send();
		
		console.log("birsanje");
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				console.log(xhttp.responseText);
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
			}
			ugasniCakanje();
		};
		
	};
	
	
	pridobiSeznamDatotek();

});
